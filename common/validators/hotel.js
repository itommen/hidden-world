import validationCalculator from './index';
import { required } from './common-validators';

export default validationCalculator({
  url: [required],
  stars: [required],
  description: [required],
  location: {
    country: [required],
    city: [required]
  },
  name: [required]
});
