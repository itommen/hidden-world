import db from '../../config/mongoose';

import { byId } from '../common/validated-query';
import ensure from '../common/validate';
import validation from '../../../common/validators/hotel';

const Hotel = db.model('Hotel');

export async function getAll() {
  return await Hotel.find();
}

export async function fetch({ params: { id } }) {
  const hotel = await byId(Hotel, id);
  return hotel;
}

export async function insert({ body }) {
  ensure(body, validation);

  const hotel = await new Hotel({ ...body }).save();

  return hotel;
}

export async function update({ body }) {
  ensure(body, validation);

  const hotel = await Hotel.findById(body.id).exec();

  hotel.$set({ ...body });

  const result = await hotel.save();

  return result;
}

export async function remove({ params: { id } }) {
  await Hotel.findByIdAndRemove(id);
}
