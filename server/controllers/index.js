// inject:route-imports
import hotel from './hotel';
import user from './user';
import countries from './country';
import tripPart from './trip-part';

export default app => {
  // inject:route-usage
  app.use('/api/hotels', hotel);
  app.use('/api/user', user);
  app.use('/api/countries', countries);
  app.use('/api/tripPart', tripPart);
};
