import mongoose from 'mongoose';

export default mongoose.Schema({
  country: {
    type: String,
    required: true
  },
  city: {
    type: String,
    required: true
  }
});
