import mongoose from 'mongoose';

import LocationSchema from '../Location';

const hotelSchema = mongoose.Schema({
  name: {
    type: String,
    required: true,
    unique: true
  },
  url: {
    type: String,
    required: true
  },
  stars: {
    type: Number,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  location: {
    type: LocationSchema,
    required: true
  }
});

export default mongoose.model('Hotel', hotelSchema);
