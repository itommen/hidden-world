import jwt from 'express-jwt';

const knownUrls = ['/api/user/login'];

export default () => jwt({ secret: process.env.SECRET })
  .unless(({ url }) => !url.startsWith('/api') || knownUrls.includes(url));
