import React from 'react';

import { without } from 'lodash';
import { Flex } from 'reflexbox';

import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Typography from '@material-ui/core/Typography';
import Checkbox from '@material-ui/core/Checkbox';

export default ({ formLabel, items = [], input: { onChange, value = [] }, style }) =>
  <FormControl style={style}>
    <Typography>{formLabel}</Typography>
    <Flex>
      {items.map(({ key, label }) => <FormControlLabel key={key}
        control={<Checkbox
          checked={value.includes(key)}
          onChange={() => {
            onChange(value.includes(key)
              ? without(value, key)
              : [...value, key]);
          }}
          value={`${key}`} />}
        label={label}
      />)}
    </Flex>
  </FormControl>;
