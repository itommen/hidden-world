import React from 'react';

import { Flex } from 'reflexbox';
import { Field } from 'redux-form';

import { withStyles } from '@material-ui/core';

import Select from 'react-select';

const styles = {
  select: {
    width: '200px'
  }
};

export default withStyles(styles)(
  ({ countries, classes, getCotunryValue, countryChanged, getCityValue, cityChanged, getCities }) => <Flex auto>
    <Field name='country'
      component={props => <Select
        className={classes.select}
        value={getCotunryValue()}
        onChange={(event) => {
          countryChanged(event);
          props.input.onChange(event.value);
        }}
        {...props}
      />}
      options={countries.map(x => ({ value: x, label: x }))} />
    <Field name='city'
      component={(props) => <Select
        className={classes.select}
        isDisabled={!getCotunryValue()}
        value={getCityValue()}
        onChange={(event) => {
          cityChanged(event);
          props.input.onChange(event.value);
        }}
        {...props} />}
      options={getCities()} />
  </Flex>
);
