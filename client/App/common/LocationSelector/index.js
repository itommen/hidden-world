import { compose, withStateHandlers, withHandlers } from 'recompose';

import { getCities } from 'countries-cities';

import LocationSelector from './LocationSelector';

export default compose(
  withStateHandlers(
    {
      selectedCountry: null,
      selectedCity: null,
      isCitySelectionDisabled: true,
      isCountryChanged: false
    },
    {
      countryChanged: state => ({ value }) => ({
        ...state,
        selectedCountry: value,
        selectedCity: null,
        isCitySelectionDisabled: false,
        isCountryChanged: true
      }),
      cityChanged: state => ({ value }) => ({ ...state, selectedCity: value })
    }
  ),
  withHandlers({
    getCurrentCoutnry: (
      { selectedCountry, data: { country } = {} }
    ) => () => selectedCountry || country
  }),
  withHandlers({
    getCities: ({ getCurrentCoutnry }) => () => {
      const selectedCountry = getCurrentCoutnry();
      return selectedCountry
        ? getCities(selectedCountry).map(x => ({ value: x, label: x })) || []
        : [];
    },
    getCotunryValue: ({ getCurrentCoutnry }) => () => {
      const country = getCurrentCoutnry();

      return country
        ? { value: country, label: country }
        : null;
    },
    getCityValue: ({ selectedCity, isCountryChanged, data: { city } = {} }) => () => selectedCity
      ? { value: selectedCity, label: selectedCity }
      : city && !isCountryChanged && { value: city, label: city }
  })
)(LocationSelector);
