import React from 'react';

import { Flex } from 'reflexbox';

import SelectFiler from '../../Filter/Filters/SelectFilter';

export default ({ countries, property, countrySelected, cityChanged, cities, city }) => <Flex>
  <SelectFiler
    options={countries}
    label={'מדינה'}
    prop={`${property}.country`}
    onChange={countrySelected}
    additionallProps={[`${property}.city`]}
    isMulti
  />
  <SelectFiler
    options={cities}
    label={'עיר'}
    prop={`${property}.city`}
    value={city && { value: city, label: city }}
    onChange={cityChanged}
    isDisabled={!cities.length}
    isMulti
  />
</Flex>;
