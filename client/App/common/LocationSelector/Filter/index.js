import { compose, withStateHandlers } from 'recompose';

import { flatten } from 'lodash';

import { getCities } from 'countries-cities';

import Filter from './Filter';

export default compose(
  withStateHandlers(
    { cities: [] },
    {
      countrySelected: state => contries => ({
        ...state,
        city: null,
        cities: contries.length
          ? [...state.cities, ...flatten(contries.map(getCities))]
          : []
      }),
      cityChanged: state => city => ({
        ...state,
        city: city.length
          ? city
          : null
      })
    }
  )
)(Filter);
