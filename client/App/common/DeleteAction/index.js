import React from 'react';

import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import dialoger, { closeDialog } from '../dialoger';

export default ({ name, onDelete, type }) => <Tooltip id='delete-icon' title='מחק'>
  <IconButton>
    <DeleteIcon onClick={() => dialoger({
      title: `מחיקת ${type}`,
      content: `האם אתה בטוח שברצונך למחוק את ה${type} ${name}?`,
      actions: [
        {
          title: 'כן',
          callback: () => {
            onDelete();
            closeDialog();
          }
        },
        {
          title: 'לא',
          callback: closeDialog
        }
      ]
    })} />
  </IconButton>
</Tooltip>;
