import React, { Fragment, Component } from 'react';

import Divider from '@material-ui/core/Divider';

export default class ExpendedFilter extends Component {
  render() {
    const { children } = this.props;
    return <Fragment>
      <Divider inset style={{ margin: '5px 0' }} />
      {children}
    </Fragment>;
  }
}
