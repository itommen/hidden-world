const CREATE_FILTER = 'CREATE_FILTER';
const REMOVE_FILTERS = 'REMOVE_FILTER';
const CLEAR_FILTERS = 'CLEAR_FILTERS';

export default (state = [], { type, filter }) => {
  switch (type) {
    case CREATE_FILTER: {
      return [...state.filter(({ prop }) => prop !== filter.prop), filter];
    }

    case REMOVE_FILTERS: {
      return [...state.filter(({ prop }) => !filter.includes(prop))];
    }

    case CLEAR_FILTERS: {
      return [];
    }

    default: {
      return state;
    }
  }
};

export const createFilter = filter => ({
  type: CREATE_FILTER,
  filter
});

export const removeFilters = filter => ({
  type: REMOVE_FILTERS,
  filter
});

export const clearFilters = () => ({
  type: CLEAR_FILTERS
});
