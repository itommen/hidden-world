import React from 'react';

import { Flex } from 'reflexbox';

import IconButton from '@material-ui/core/IconButton';
import ArrowDropDownCircleIcon from '@material-ui/icons/ArrowDropDownCircle';

import AnimateHeight from 'react-animate-height';

export const FilterContext = React.createContext();

export default ({
  previdewFilters,
  expentedFilters,
  isAnyExpendedFilter,
  add,
  remove,
  isExpendedFilterOpen,
  toggleExpenededFilter
}) =>
  <Flex>
    <FilterContext.Provider value={{ add, remove }}>
      <Flex auto column>
        <Flex align='center'>
          {isAnyExpendedFilter
            ? <IconButton onClick={toggleExpenededFilter}>
              <ArrowDropDownCircleIcon
                className={isExpendedFilterOpen ? 'circle-icon circle-icon-flip' : 'circle-icon'}
              />
            </IconButton>
            : null
          }
          {previdewFilters}
        </Flex>

        <AnimateHeight
          animateOpacity={true}
          duration={300}
          height={isExpendedFilterOpen ? 'auto' : 0}>
          {expentedFilters}
        </AnimateHeight>

      </Flex>
    </FilterContext.Provider>
  </Flex >;
