import React from 'react';

import { get } from 'lodash';

import { Flex } from 'reflexbox';

import TextField from '@material-ui/core/TextField';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';

import { FilterContext } from '../Filter';

const styles = {
  textField: {
    width: '200px'
  }
};

export default withStyles(styles)(
  ({ label, prop, classes, additionallProps = [], ...props }) =>
    <FilterContext.Consumer>
      {
        filter =>
          <Flex column>
            <Typography>{label}</Typography>
            <TextField
              className={classes.textField}
              onChange={({ target: { value } }) => {
                if (value) {
                  filter.add(prop, x => get(x, prop).includes(value));
                } else {
                  filter.remove([prop, ...additionallProps]);
                }
              }
              }
              {...props}
            />
          </Flex>
      }
    </FilterContext.Consumer>
);
