import React from 'react';

import { get } from 'lodash';

import { Flex } from 'reflexbox';

import Select from 'react-select';

import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core';

import { FilterContext } from '../Filter';

const styles = {
  select: {
    width: '200px'
  }
};

export default withStyles(styles)(
  ({ options, label, prop, classes, onChange = () => { },
    isMulti, additionallProps = [], ...props }) =>
    <FilterContext.Consumer>
      {
        filter =>
          <Flex column>
            <Typography>{label}</Typography>
            <Select
              className={classes.select}
              isClearable={true}
              options={[...options.map(x => ({ label: x, value: x }))]}
              onChange={(rowValues, { action }) => {
                let values = [];

                if (isMulti) {
                  values = rowValues.map(({ value }) => value);
                } else if (rowValues) {
                  values = [rowValues.value];
                }

                onChange(values);

                if (action === 'clear' || (action === 'remove-value' && !values.length)) {
                  filter.remove([prop, ...additionallProps]);
                } else {
                  filter.add(prop, x => values.includes(get(x, prop)));
                }
              }
              }
              isMulti={isMulti}
              {...props}
            />
          </Flex>
      }
    </FilterContext.Consumer>
);
