import React from 'react';

import { connect } from 'react-redux';

import { compose, lifecycle, withProps, withStateHandlers } from 'recompose';

import { clearFilters, createFilter, removeFilters } from './redux';

import Filter from './Filter';

import ExpendedFilter from './ExpendedFilter';

import './style.less';

export default compose(
  connect(
    () => ({}),
    dispatch => ({
      clear: () => dispatch(clearFilters()),
      add: (prop, func) => dispatch(createFilter({
        prop,
        func
      })),
      remove: props => dispatch(removeFilters(props))
    })
  ),
  lifecycle({
    componentWillUnmount() {
      const { clear } = this.props;
      clear();
    }
  }),
  withStateHandlers(
    { isExpendedFilterOpen: false },
    {
      toggleExpenededFilter: (state) => () => ({
        ...state,
        isExpendedFilterOpen: !state.isExpendedFilterOpen
      })
    }
  ),
  withProps((props) => {
    const childrens = React.Children.toArray(props.children);

    const isExpendedFilter = ({ type: { prototype } }) => prototype instanceof ExpendedFilter;

    const expentedFilters = childrens.filter(isExpendedFilter);
    const previdewFilters = childrens.filter(x => !isExpendedFilter(x));
    const isAnyExpendedFilter = childrens.some(isExpendedFilter);

    return {
      previdewFilters,
      expentedFilters,
      isAnyExpendedFilter
    };
  })
)(Filter);
