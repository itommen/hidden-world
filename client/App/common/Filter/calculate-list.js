import { store } from '../../../common/store';

export default data => {
  const { filter } = store.getState();

  return data && filter.reduce((result, current) => result.filter(current.func), data) || [];
};
