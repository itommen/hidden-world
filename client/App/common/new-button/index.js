import React from 'react';

import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';

import redirect from '../../common/navigation';

export default ({ path }) => <Tooltip id='add-icon' title='הוסף'>
  <Button variant='fab'
    mini
    color='secondary'
    style={{
      zIndex: 1,
      position: 'fixed',
      bottom: '2%',
      left: '2%'
    }}
    onClick={() => redirect(path)} >
    <AddIcon />
  </Button>
</Tooltip>;
