import React from 'react';
import { Field } from 'redux-form';

import ReactQuill from './ReduxFormDraft';

export default ({ style = {}, name }) => <Field
  style={style}
  name={name}
  component={ReactQuill} />;
