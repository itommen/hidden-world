import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Checkbox from '@material-ui/core/Checkbox';

import { Flex } from 'reflexbox';

import TablePagination from '@material-ui/core/TablePagination';
import TablePaginationActions from './TablePaginationActions';

export default class CountriesTable extends Component {
  constructor() {
    super();

    this.state = {
      page: 0,
      rowsPerPage: 0
    };

    this.tableBodyRef = null;
    this.tableHeadRef = null;

    this.handleChangePage = this.handleChangePage.bind(this);
    this.handleChangeRowsPerPage = this.handleChangeRowsPerPage.bind(this);
  }

  componentDidMount() {
    const { clientHeight: tableHeight } = ReactDOM.findDOMNode(this.tableBodyRef);
    const { clientHeight: tableHeadHeight } = ReactDOM.findDOMNode(this.tableHeadRef);

    this.setState(state => ({
      ...state,
      rowsPerPage: Math.round((tableHeight - tableHeadHeight) / 48) - 1
    }));
  }

  handleChangePage(event, page) {
    this.setState({ page });
  }

  handleChangeRowsPerPage(event) {
    this.setState({ rowsPerPage: event.target.value });
  }

  render() {
    const { countries, onCellClicked } = this.props;
    const { page, rowsPerPage } = this.state;

    const emptyRows = rowsPerPage - Math.min(rowsPerPage, countries.length - page * rowsPerPage);

    return <Flex auto style={{ overflowY: 'auto', minHeight: 0 }} ref={element => {
      this.tableBodyRef = element;
    }}>
      <Table style={{ direction: 'ltr' }} ref={element => {
        this.tableHeadRef = element;
      }}>
        <TableHead>
          <TableRow>
            <TableCell padding='none' style={{ width: '10%' }} />
            <TableCell>Name</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {
            countries.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map(x =>
              <TableRow key={x}>
                <TableCell padding='none'>
                  <Checkbox
                    onChange={() => onCellClicked(x)} />
                </TableCell>
                <TableCell>{x}</TableCell>
              </TableRow>)
          }
          {emptyRows > 0
            ? <TableRow style={{ height: 48 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
            : null
          }
          <TableRow>
            <TablePagination
              colSpan={3}
              count={countries.length}
              rowsPerPage={rowsPerPage}
              page={page}
              onChangePage={this.handleChangePage}
              onChangeRowsPerPage={this.handleChangeRowsPerPage}
              rowsPerPageOptions={[]}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableBody>
      </Table>
    </Flex>;
  }
}
