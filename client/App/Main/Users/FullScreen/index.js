import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import alerter from '../../../common/alerter';
import redirect from '../../../common/navigation';

import FullScreen from './FullScreen';

import { fetchUser } from '../redux';

export default compose(
  connect(
    ({ users }, { params: { id } }) => ({
      data: users[id],
      loaded: !!users[id]
    }),
    (dispath, { params: { id } }) => ({
      fetch: async function () {
        const { error } = await dispath(fetchUser(id));

        if (error) {
          alerter({
            message: 'The requested user not found'
          });

          redirect('/users');
        }
      }
    })
  ),
  lifecycle({
    componentDidMount() {
      const { fetch } = this.props;
      fetch();
    }
  })
)(FullScreen);
