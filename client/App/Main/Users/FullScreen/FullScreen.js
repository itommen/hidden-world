import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

import { Flex } from 'reflexbox';

export default ({ data: { userName, firstName, lastName, email } = {}, loaded }) => !loaded
  ? <CircularProgress />
  : <Flex column auto p={2}>
      <Typography variant='display3'>{userName}</Typography >
      <Flex column auto justify='space-around'>
        <Flex column>
          <Typography variant='title'>שם פרטי:</Typography>
          <Typography variant='subheading'>{firstName}</Typography>
        </Flex>
        <Flex column>
          <Typography variant='title'>שם משפחה:</Typography>
          <Typography variant='subheading'>{lastName}</Typography>
        </Flex>
        <Flex column>
          <Typography variant='title'>דוא"ל:</Typography>
          <Typography variant='subheading'>{email}</Typography>
        </Flex>
      </Flex>
  </Flex >;
