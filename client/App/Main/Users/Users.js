import React, { Fragment } from 'react';

import List from '@material-ui/core/List';

import ListItem from './ListItem';

import NewButton from '../../common/new-button';

export default ({ users = [], onDelete }) => <Fragment>
  <List>
    {users.map(({ ...props }) => <ListItem key={props.id} {...props} onDelete={onDelete} />)}
  </List>
  <NewButton path={'/users/new/'} />
</Fragment>;
