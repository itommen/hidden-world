import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import alerter from '../../common/alerter';

import Users from './Users';
import { loadUsers, remove } from './redux';

export default compose(
  connect(
    ({ users: { data } = {} }) => ({ users: data }),
    dispath => ({
      loadData: async function () {
        const { error } = await dispath(loadUsers());

        if (error) {
          alerter({
            message: 'טעינת המשתמשים נכשלה'
          });
        }
      },
      onDelete: async function (id) {
        const { error } = await dispath(remove(id));

        alerter({
          message: error
            ? 'המחיקה נכשלה'
            : 'המחיקה הסתיימה בהצלחה'
        });
      }
    })
  ),
  lifecycle({
    componentDidMount() {
      const { loadData } = this.props;
      loadData();
    }
  })
)(Users);
