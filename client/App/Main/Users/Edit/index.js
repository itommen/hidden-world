import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import alerter from '../../../common/alerter';
import redirect from '../../../common/navigation';

import Edit from './Edit';

import { editUser, fetchUser } from '../redux';

export default compose(
  connect(
    ({ users }, { params: { id } }) => ({
      data: users[id],
      action: editUser
    }),
    (dispath, { params: { id } }) => ({
      fetch: async function () {
        const { error } = await dispath(fetchUser(id));

        if (error) {
          alerter({
            message: 'המשתמש לא נמצא'
          });

          redirect('/users');
        }
      }
    })
  ),
  lifecycle({
    componentDidMount() {
      const { fetch } = this.props;
      fetch();
    }
  })
)(Edit);
