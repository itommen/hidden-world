import React from 'react';

import { Flex } from 'reflexbox';

import { Field } from 'redux-form';
import { TextField } from 'redux-form-material-ui';

import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import { withStyles } from '@material-ui/core';

const styles = {
  cardLayout:{
    width:'35%'
  }
};

const EditComponent = ({ handleSubmit, title, classes }) => <Flex column auto justify='center'>
  <Flex justify='center'>
    <Card className={classes.cardLayout}>
      <CardHeader title={title} />
      <CardContent>
        <Flex column>
          <form onSubmit={handleSubmit}>
            <Flex column>
              <Field name='userName'
                component={TextField}
                label='שם משתמש' />

              <Field name='password'
                component={TextField}
                label='סיסמא' />

              {/* TODO: retype password  */}

              <Field name='email'
                component={TextField}
                label='אימייל' />

              <Field name='firstName'
                component={TextField}
                label='שם פרטי' />

              <Field name='lastName'
                component={TextField}
                label='שם משפחה' />

              <Flex justify='center' mt={3}>
                <Button
                  variant='contained'
                  type='submit'
                  name='editUser'
                  color='secondary'>
                  שמור
                </Button>
              </Flex>
            </Flex>
          </form>
        </Flex>
      </CardContent>
    </Card>
  </Flex>
</Flex>;

export default withStyles(styles)(EditComponent);
