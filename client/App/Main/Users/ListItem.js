import React from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import LeftListItemSecondaryAction from '../../common/LeftListItemSecondaryAction';
import ModeEditIcon from '@material-ui/icons/ModeEdit';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import redirect from '../../common/navigation';
import DeleteAction from '../../common/DeleteAction';

export default ({ id, lastName, firstName, userName, onDelete }) => <ListItem button
  divider={true}
  onClick={() => redirect(`/users/${id}`)}>
  <ListItemText
    style={{ textAlign: 'right' }}
    primary={`${firstName} ${lastName}`}
    secondary={userName} />
  <LeftListItemSecondaryAction>
    <Tooltip id='edit-icon' title='ערוך'>
      <IconButton>
        <ModeEditIcon onClick={() => redirect(`/users/${id}/edit`)} />
      </IconButton>
    </Tooltip>
    <DeleteAction onDelete={() => onDelete(id)} name={userName} type={'משתמש'} />
  </LeftListItemSecondaryAction>
</ListItem>;
