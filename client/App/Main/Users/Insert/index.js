import React from 'react';

import EditComponent from '../EditComponent';

import { addUser } from '../redux';

export default () => <EditComponent
  title={'הוספת משתמש'}
  action={addUser}
  errorMessage={'Insert failed'}
/>;
