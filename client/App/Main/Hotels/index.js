import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import alerter from '../../common/alerter';

import filterList from '../../common/Filter/calculate-list';

import Hotels from './Hotels';
import { loadHotels, removeHotel } from './redux';

export default compose(
  connect(
    ({ hotels: { data } = {},
      countries: { relevant = [] } = {},
      filter }) =>
      ({
        hotels: filterList(data),
        countries: relevant
      }),
    dispath => ({
      loadData: async function () {
        const { error } = await dispath(loadHotels());

        if (error) {
          alerter({
            message: 'הטעינה נכשלה'
          });
        }
      },
      onDelete: async function (id) {
        const { error } = await dispath(removeHotel(id));

        alerter({
          message: error
            ? 'המחיקה נכשלה'
            : 'המחיקה הסתיימה בהצלחה'
        });
      }
    })
  ),
  lifecycle({
    componentDidMount() {
      const { loadData } = this.props;
      loadData();
    }
  })
)(Hotels);
