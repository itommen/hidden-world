import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';

import EditComponent from '../EditComponent';

export default ({ action, data }) => data
  ? <EditComponent
    title={'עריכת מלון'}
    action={action}
    data={data}
    errorMessage={'Edit failed'}
  />
  : <CircularProgress />;
