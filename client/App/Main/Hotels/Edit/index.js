import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import alerter from '../../../common/alerter';
import redirect from '../../../common/navigation';

import Edit from './Edit';

import { editHotel, fetchHotel } from '../redux';

export default compose(
  connect(
    ({ hotels }, { params: { id } }) => ({
      data: hotels[id],
      action: editHotel
    }),
    (dispath, { params: { id } }) => ({
      fetch: async function () {
        const { error } = await dispath(fetchHotel(id));

        if (error) {
          alerter({
            message: 'המשתמש לא נמצא'
          });

          redirect('/hotels');
        }
      }
    })
  ),
  lifecycle({
    componentDidMount() {
      const { fetch } = this.props;
      fetch();
    }
  })
)(Edit);
