import { createAction } from 'redux-actions';
import { resolve, reject } from 'redux-simple-promise';

import {
  started as deleteStarted,
  resolved as deleteResolved,
  rejected as deleteRejected
} from '../../common/redux-actions/delete';

export const LOAD = 'LOAD_HOTELS';
export const INSERT = 'INSERT_HOTEL';
export const EDIT = 'EDIT_HOTEL';
export const FETCH = 'FETCh_HOTEL';
export const REMOVE = 'REMOVE_HOTEL';

const internalState = {
  loading: false,
  deleted: []
};

export default (state = internalState, { type, payload: { data, request } = {}, meta }) => {
  switch (type) {
    case LOAD: {
      return { ...state, loading: true };
    }

    case resolve(LOAD): {
      return { ...state, data, loading: false };
    }

    case reject(LOAD): {
      return { ...state, loading: false };
    }

    case resolve(FETCH): {
      const { id } = data;
      return { ...state, [id]: data };
    }

    case resolve(EDIT): {
      const { id } = data;
      return { ...state, [id]: data };
    }

    case resolve(INSERT): {
      return { ...state, data: [...state.data, data] };
    }
    case REMOVE: {
      return deleteStarted(state, request);
    }

    case resolve(REMOVE): {
      return deleteResolved(state, meta);
    }

    case reject(REMOVE): {
      return deleteRejected(state, meta);
    }

    default: {
      return state;
    }
  }
};

export const loadHotels = createAction(LOAD, () => ({
  request: {
    url: '/hotels',
    method: 'GET'
  }
})
);

export const fetchHotel = createAction(FETCH, id => ({
  request: {
    url: `/hotels/${id}`,
    method: 'GET'
  }
})
);

export const removeHotel = createAction(REMOVE, id => ({
  request: {
    url: `/hotels/${id}`,
    method: 'DELETE',
    id
  }
})
);

export const editHotel = createAction(EDIT, data => ({
  request: {
    url: '/hotels',
    method: 'PUT',
    data
  }
}));

export const insertHotel = createAction(INSERT, data => ({
  request: {
    url: '/hotels',
    method: 'POST',
    data
  }
})
);
