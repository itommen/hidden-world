import React from 'react';

import EditComponent from '../EditComponent';

import { insertHotel } from '../redux';

export default () => <EditComponent
  title={'הוספת מלון'}
  action={ insertHotel }
  errorMessage={'Insert failed'}
/>;
