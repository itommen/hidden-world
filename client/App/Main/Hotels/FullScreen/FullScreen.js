import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

import { Flex } from 'reflexbox';

import { Parser } from 'html-to-react';

import StarsRate from '../../../common/StarsRate';

import formatPlace from '../../../common/format-location';

export default ({ data: { url, stars, description, location, name } = {}, loaded }) => !loaded
  ? <CircularProgress />
  : <Flex column auto p={2}>
    <Flex>
      <Typography variant='display3'>{name}</Typography >
      <StarsRate rate={stars} />
    </Flex>
    <Flex column auto justify='space-around'>
      <Flex column>
        <Typography variant='title'>לינק:</Typography>
        <Typography variant='subheading'>{url}</Typography>
      </Flex>
      <Flex column>
        <Typography variant='title'>תיאור:</Typography>
        {new Parser().parse(description)}
      </Flex>
      <Flex column>
        <Typography variant='title'>מיקום:</Typography>
        <Typography variant='subheading'>{formatPlace(location)}</Typography>
      </Flex>
    </Flex>
  </Flex >;
