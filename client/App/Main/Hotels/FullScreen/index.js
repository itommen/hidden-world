import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import alerter from '../../../common/alerter';
import redirect from '../../../common/navigation';

import FullScreen from './FullScreen';

import { fetchHotel } from '../redux';

export default compose(
  connect(
    ({ hotels }, { params: { id } }) => ({
      data: hotels[id],
      loaded: !!hotels[id]
    }),
    (dispath, { params: { id } }) => ({
      fetch: async function () {
        const { error } = await dispath(fetchHotel(id));

        if (error) {
          alerter({
            message: 'The requested Hotel not found'
          });

          redirect('/hotels');
        }
      }
    })
  ),
  lifecycle({
    componentDidMount() {
      const { fetch } = this.props;
      fetch();
    }
  })
)(FullScreen);
