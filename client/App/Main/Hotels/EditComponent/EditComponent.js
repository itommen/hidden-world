import React from 'react';

import { Flex } from 'reflexbox';

import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';

import { Field, FormSection } from 'redux-form';
import { TextField, Select } from 'redux-form-material-ui';
import { withStyles } from '@material-ui/core';

import LocationSelector from '../../../common/LocationSelector';
import StarsRate from '../../../common/StarsRate';
import ReduxFormDraft from '../../../common/ReduxFormDraft';

const styles = {
  cardLayout: {
    width: '50%',
    margin: 8
  },
  formLayout: {
    display: 'flex',
    flex: '1 1 auto'
  },
  stretch: {
    width: '100%'
  },
  stars: {
    marginTop: -5,
    marginRight: 5
  },
  description: {
    overflow: 'auto',
    maxHeight: '30%'
  }
};

const EditComponent = ({ title, handleSubmit, countries, data = {}, location, classes }) => <Flex column auto justify='center'>
  <Flex justify='center'>
    <Card className={classes.cardLayout}>
      <CardHeader title={title}></CardHeader>
      <CardContent>
        <Flex column auto>
          <form onSubmit={handleSubmit} className={classes.formLayout}>
            <Flex column auto>
              <Flex column auto>
                <Flex>
                  <Flex auto>
                    <Field name='name' className={classes.stretch}
                      component={TextField}
                      label='שם המלון' />
                  </Flex>

                  <Flex className={classes.stars}>
                    <FormControl>
                      <InputLabel>כוכבים</InputLabel>
                      <Field name='stars'
                        component={Select}>
                        {[1, 2, 3, 4, 5].map(x => <MenuItem key={`item${x}`} value={x}>
                          <StarsRate rate={x} />
                        </MenuItem>)}
                      </Field>
                    </FormControl>
                  </Flex>
                </Flex>

                <Field name='url'
                  component={TextField}
                  label='לינק' />
                <FormSection name='location'>
                  <Flex>
                    <Typography>מיקום המלון</Typography>
                    <LocationSelector countries={countries} data={data.location} />
                  </Flex>
                </FormSection>

                <ReduxFormDraft name={'description'} className={classes.description} />

              </Flex>
              <Flex justify='center'>
                <Button
                  variant='contained'
                  type='submit'
                  name='login'
                  color='secondary'>
                  שמור
                </Button>
              </Flex>
            </Flex>
          </form>
        </Flex>
      </CardContent>
    </Card>
  </Flex>
</Flex>;

export default withStyles(styles)(EditComponent);
