import { connect } from 'react-redux';

import { compose, lifecycle } from 'recompose';

import { reduxForm } from 'redux-form';

import alerter from '../../../common/alerter';
import redirect from '../../../common/navigation';

import validate from '../../../../../common/validators/hotel';

import EditComponent from './EditComponent';

export default compose(
  connect(
    ({ countries: { relevant = [] } = {} }) => ({
      countries: relevant
    }),
    (dispath, { action, errorMessage }) => ({
      onSubmit: async function (result) {
        const { error } = await dispath(action(result));

        if (error) {
          alerter({
            message: errorMessage
          });
        } else {
          redirect('/hotels');
        }
      }
    })
  ),
  reduxForm({
    form: 'editHotelForm',
    validate,
    initialValues: {
      stars: 1
    }
  }),
  lifecycle({
    componentDidMount() {
      const { data, initialize } = this.props;

      if (data) {
        initialize(data);
      }
    }
  })
)(EditComponent);
