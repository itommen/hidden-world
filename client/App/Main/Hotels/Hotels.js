import React, { Fragment } from 'react';

import List from '@material-ui/core/List';

import ListItem from './ListItem';

import Filter from '../../common/Filter';
import ExpendedFilter from '../../common/Filter/ExpendedFilter';
import TextFilter from '../../common/Filter/Filters/TextFilter';
import LocationFilter from '../../common/LocationSelector/Filter';

import NewButton from '../../common/new-button';

export default ({ hotels, onDelete, countries }) => <Fragment>
  <Filter>
    <LocationFilter
      countries={countries}
      property={'location'}
    />
    <ExpendedFilter>
      <TextFilter
        label={'תיאור'}
        prop={'description'}
      />
    </ExpendedFilter>
  </Filter>
  <List>
    {hotels.map(({ ...props }) => <ListItem key={props.id} {...props} onDelete={onDelete} />)}
  </List>
  <NewButton path={'/hotels/new/'} />
</Fragment>;
