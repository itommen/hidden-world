import React from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import LeftListItemSecondaryAction from '../../common/LeftListItemSecondaryAction';
import ModeEditIcon from '@material-ui/icons/ModeEdit';
import IconButton from '@material-ui/core/IconButton';

import DeleteAction from '../../common/DeleteAction';

import redirect from '../../common/navigation';

export default ({ id, name, onDelete }) => <ListItem button
  divider={true}
  onClick={() => redirect(`/hotels/${id}`)}>
  <ListItemText
    style={
      { textAlign: 'right' }
    }
    primary={`${name}`}
    secondary={`${name}`} />
  <LeftListItemSecondaryAction>
    <IconButton>
      <ModeEditIcon onClick={() => redirect(`/hotels/${id}/edit`)} />
    </IconButton>
    <DeleteAction onDelete={() => onDelete(id)} name={name} type={'מלון'} />
  </LeftListItemSecondaryAction>
</ListItem>;
