import React from 'react';
import { Route, IndexRoute } from 'react-router';

import Home from './Home';

// inject:route-imports
import HotelsRoutes from './Hotels/routes';
import TripPartRoutes from './TripPart/routes';
import ManageCountriesRoutes from './ManageCountries/routes';
import UsersRoutes from './Users/routes';

import loadInternalData from './load-internal-data';

export default <Route onEnter={loadInternalData}>
  <IndexRoute component={Home} />
  // inject:route-usage
  { HotelsRoutes }
  {TripPartRoutes}
  {ManageCountriesRoutes}
  {UsersRoutes}
</Route>;
