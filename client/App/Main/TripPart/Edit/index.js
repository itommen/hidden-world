import { connect } from 'react-redux';
import { compose, lifecycle } from 'recompose';

import alerter from '../../../common/alerter';
import redirect from '../../../common/navigation';

import Edit from './Edit';

import { editTripPart, fetchTripPart } from '../redux';

export default compose(
  connect(
    ({ tripParts }, { params: { tripPartId } }) => ({
      data: tripParts[tripPartId],
      action: editTripPart
    }),
    (dispath, { params: { tripPartId } }) => ({
      fetch: async function () {
        const { error } = await dispath(fetchTripPart(tripPartId));

        if (error) {
          alerter({
            message: 'חלק הטיול לא נמצא'
          });

          redirect('/tripPart');
        }
      }
    })
  ),
  lifecycle({
    componentDidMount() {
      const { fetch } = this.props;
      fetch();
    }
  })
)(Edit);
