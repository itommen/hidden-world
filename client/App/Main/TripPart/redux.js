import { createAction } from 'redux-actions';
import { resolve, reject } from 'redux-simple-promise';

import {
  started as deleteStarted,
  resolved as deleteResolved,
  rejected as deleteRejected
} from '../../common/redux-actions/delete';

export const LOAD = 'LOAD_TRIP_PARTS';
export const INSERT = 'INSERT_TRIP_PARTS';
export const EDIT = 'EDIT_TRIP_PARTS';
export const FETH = 'FETH_TRIP_PAART';
export const DELETE = 'DELETE_TRIP_PART';

const internalState = {
  loading: false,
  data: [],
  deleted: []
};

export default (state = internalState, { type, payload: { data, request } = {}, meta }) => {
  switch (type) {
    case LOAD: {
      return { ...state, loading: true };
    }

    case resolve(LOAD): {
      return { ...state, data, loading: false };
    }

    case resolve(FETH): {
      const { id } = data;
      return { ...state, [id]: data };
    }

    case resolve(INSERT): {
      return { ...state, data: [...state.data, data] };
    }

    case resolve(EDIT): {
      const { id } = data;
      return { ...state, [id]: data };
    }

    case DELETE: {
      return deleteStarted(state, request);
    }

    case resolve(DELETE): {
      return deleteResolved(state, meta);
    }

    case reject(DELETE): {
      return deleteRejected(state, meta);
    }

    default: {
      return state;
    }
  }
};

export const loadTripParts = createAction(LOAD, () => ({
  request: {
    url: '/tripPart',
    method: 'GET'
  }
})
);

export const fetchTripPart = createAction(FETH, id => ({
  request: {
    url: `/tripPart/${id}`,
    method: 'GET'
  }
})
);

export const deleteTripPart = createAction(DELETE, id => ({
  request: {
    url: `/tripPart/${id}`,
    method: 'DELETE',
    id
  }
})
);

export const insertTripPart = createAction(INSERT, data => ({
  request: {
    url: '/tripPart',
    method: 'POST',
    data
  },
  client: 'withFiles'
})
);

export const editTripPart = createAction(EDIT, data => ({
  request: {
    url: '/tripPart',
    method: 'PUT',
    data
  },
  client: 'withFiles'
})
);
