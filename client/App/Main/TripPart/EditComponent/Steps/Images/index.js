import React from 'react';

import { Field } from 'redux-form';

import ImageUploader from './ImageUploader';

export default ({ savedImages = [], style }) => <Field name='images'
  style={style}
  component={ImageUploader}
  images={savedImages}
/>;
