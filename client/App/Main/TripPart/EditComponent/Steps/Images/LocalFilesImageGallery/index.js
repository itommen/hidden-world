import { compose, withHandlers, lifecycle } from 'recompose';

import { readAsDataURL } from 'promise-file-reader';

import LocalFilesImageGallery from './LocalFilesImageGallery';

export default compose(
  withHandlers({
    getGaleryImages: ({ files = [] }) => async () => {
      if (!files || !files.length) {
        return;
      }

      const images = await Promise.all([...files].map(readAsDataURL));

      return images.map(x => ({
        original: x,
        thumbnail: x
      }));
    }
  }),
  lifecycle({
    async componentDidMount() {
      const { savedImages = [], getGaleryImages } = this.props;

      const images = await getGaleryImages() || [];

      const savedImagesInDisplayFormat = savedImages.map(x => ({
        original: `/uploads/${x}`,
        thumbnail: `/uploads/${x}`
      }));

      this.setState(state => ({
        ...state,
        images,
        savedImages: savedImagesInDisplayFormat,
        items: [...savedImagesInDisplayFormat, ...images]
      }));
    },

    async componentDidUpdate({ files }) {
      const { getGaleryImages } = this.props;
      if (files !== this.props.files) {
        const images = await getGaleryImages();

        this.setState(state => ({
          ...state,
          images,
          items: [...state.savedImages, ...images]
        }));
      }
    }
  })
)(LocalFilesImageGallery);
