import React from 'react';

import ImageGallery from 'react-image-gallery';

export default ({ items = [] }) => items.length
  ? <ImageGallery
    items={items}
    showPlayButton={false}
    showIndex={true} />
  : null;
