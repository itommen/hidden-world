import React from 'react';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardHeader from '@material-ui/core/CardHeader';

import LocalFilesImageGallery from './LocalFilesImageGallery';

export default ({ input: { onChange, value }, images, style }) => <Card style={style}>
  <CardHeader title='תמונות מהיום' subheader={<input type='file'
    multiple
    accept='image/*'
    onChange={({ target: { files } }) => onChange(files)} />} />
  <CardContent>
    <LocalFilesImageGallery files={value} savedImages={images} />
  </CardContent>
</Card>;
