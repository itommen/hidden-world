import React from 'react';

import FormControl from '@material-ui/core/FormControl';
import { Field, FormSection } from 'redux-form';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';

import LocationSelector from '../../../../../common/LocationSelector';

import { TextField } from 'redux-form-material-ui';

import { Flex } from 'reflexbox';

import CheckboxGroup from '../../../../../common/CheckboxGroup';
import { domestic, foreign } from '../../../flight-type.const';

export default ({ countries, start, end, style }) => <Card style={style}>
  <CardContent style={style}>
    <Flex auto justify='space-around' column>
      <Field name='name'
        label='שם ליום טיול'
        component={TextField} />
      <Flex auto>
        <FormSection name='start' >
          <Flex auto column style={{ marginTop: 15, marginLeft: 30 }}>
            <Typography>יציאה מ</Typography>
            <LocationSelector countries={countries} data={start} />
          </Flex>
        </FormSection>

        <FormSection name='end'>
          <Flex auto column style={{ marginTop: '15px' }}>
            <Typography>הגעה ל</Typography>
            <LocationSelector countries={countries} data={end} />
          </Flex>
        </FormSection>
      </Flex>
      <Field name='flight'
        style={{ marginTop: 15 }}
        component={CheckboxGroup}
        items={[{ key: domestic, label: 'פנים' }, { key: foreign, label: 'חוץ' }]}
        formLabel='טיסות'
      />

      <Field name='days'
        component={TextField}
        type='number'
        InputProps={{ inputProps: { min: 1 } }}
        label='כמה ימים' />
    </Flex>
  </CardContent>
</Card>;
