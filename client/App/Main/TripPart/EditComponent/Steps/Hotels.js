import React from 'react';

import { Field } from 'redux-form';
import { Flex } from 'reflexbox';

import FeaturedHotels from '../FeaturedHotels';

export default ({ style }) => <Flex style={style} justify='center'>
  <Field name='hotels'
    component={FeaturedHotels} />
</Flex>;
