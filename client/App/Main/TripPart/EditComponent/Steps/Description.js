import React from 'react';

import ReduxFormDraft from '../../../../common/ReduxFormDraft';

export default ({ style }) => <ReduxFormDraft
  style={style}
  name={'description'} />;
