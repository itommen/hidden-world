import React from 'react';

import { keys } from 'lodash';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import StepButton from '@material-ui/core/StepButton';
import StepLabel from '@material-ui/core/StepLabel';

import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';

import { Flex } from 'reflexbox';

import { GeneralInfo, Description, Images, Hotels } from './Steps';

const steps = [
  {
    label: 'פרטים כללים',
    fields: ['name', 'start', 'end', 'flight', 'days']
  },
  {
    label: 'מלונות',
    fields: ['hotels']
  },
  {
    label: 'תיאור היום',
    fields: ['description']
  },
  {
    label: 'תמונות מהיום',
    fields: ['images']
  }
];

export default ({ handleSubmit, countries, submitting, errors, meta, data = {}, activeStep, handleMoveStep }) => <Flex auto>
  <form onSubmit={handleSubmit} style={{
    display: 'flex',
    flex: '1 1 auto',
    flexDirection: 'column'
  }}>
    <Typography variant='display2'>יום טיול</Typography>
    <Stepper nonLinear activeStep={activeStep} style={{ backgroundColor: 'inherit' }}>
      {
        steps.map(({ label, fields }, index) => <Step key={label}>
          <StepButton
            onClick={() => handleMoveStep(index)}
            completed={!fields.some(x => keys(errors).includes(x))}>
            <StepLabel error={
              fields.some(x => keys(errors).includes(x) && ((meta || {})[x] || {}).touched)
            }>
              {label}
            </StepLabel>
          </StepButton>
        </Step>)
      }
    </Stepper>
    <Flex column auto justify='space-between' style={{ height: '100%' }}>
      <Flex style={{ height: '85%', width: '100%' }}>
        <GeneralInfo start={data.start} end={data.end} countries={countries} style={{
          display: activeStep === 0 ? '' : 'none',
          width: '100%'
        }} />
        <Hotels style={{
          display: activeStep === 1 ? '' : 'none',
          width: '100%'
        }} />
        <Description style={{
          display: activeStep === 2 ? '' : 'none',
          overflow: 'auto',
          width: '100%'
        }} />
        <Images savedImages={data.savedImages} style={{
          display: activeStep === 3 ? '' : 'none',
          overflow: 'auto',
          width: '100%'
        }} />
      </Flex>
      <Flex justify='space-around'>
        <IconButton aria-label='moveRight'
          disabled={submitting || activeStep === 0}
          onClick={() => handleMoveStep(activeStep - 1)}>
          <KeyboardArrowRight style={{ fontSize: 36 }} />
        </IconButton>
        <Button
          variant='contained'
          type='submit'
          name={'login'}
          disabled={submitting}
          color='secondary'>
          שמור
            </Button>
        <IconButton aria-label='moveLeft'
          name={'login'}
          disabled={submitting || activeStep === steps.length - 1}
          onClick={() => handleMoveStep(activeStep + 1)}>
          <KeyboardArrowLeft style={{ fontSize: 36 }} />
        </IconButton>
      </Flex>
    </Flex>
  </form>
</Flex>;
