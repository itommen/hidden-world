import React from 'react';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import { Flex } from 'reflexbox';

import Typography from '@material-ui/core/Typography';

import StarsRate from '../../../../common/StarsRate';

export default ({ name, link, stars, children }) => <Card style={{
  margin: '10px',
  minWidth: '130px'
}}>
  <CardContent>
    <Flex align='left'>
      {children}
    </Flex>
    <Flex justify='space-around'>
      <Typography variant='display1'
        onClick={() => window.open(link)}>{name}</Typography>
    </Flex>
    <Flex justify='space-around'>
      <StarsRate rate={stars} />
    </Flex>
  </CardContent>
</Card>;
