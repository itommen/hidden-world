import { connect } from 'react-redux';

import { compose, lifecycle } from 'recompose';

import { reduxForm, reset } from 'redux-form';
import uuid from 'uuid/v4';

import validate from '../../../../../../../common/validators/newFeaturedHotel';

import EditComponent from './EditComponent';

const FORM_NAME = 'feturedHotelForm';

export default compose(
  connect(
    () => ({}),
    (dispath, { save, close }) => ({
      onSubmit: (data) => {
        save({
          ...data,
          id: data.id || uuid()
        });
        dispath(reset(FORM_NAME));
      },
      closeDialog: () => {
        close();
        dispath(reset(FORM_NAME));
      }
    })
  ),
  reduxForm({
    form: FORM_NAME,
    validate,
    initialValues: {
      stars: 3
    }
  }),
  lifecycle({
    componentDidMount() {
      const { data, initialize } = this.props;

      if (data) {
        initialize(data);
      }
    }
  })
)(EditComponent);
