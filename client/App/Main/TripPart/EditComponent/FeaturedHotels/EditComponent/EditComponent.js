import React from 'react';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import { TextField, Select } from 'redux-form-material-ui';
import Button from '@material-ui/core/Button';

import { Flex } from 'reflexbox';

import { Field } from 'redux-form';

import StarsRate from '../../../../../common/StarsRate';

export default ({ isOpen, closeDialog, handleSubmit }) => <Dialog
  aria-labelledby='form-dialog-title'
  open={isOpen}>
  <DialogTitle>הוספת מלון</DialogTitle>
  <form>
    <DialogContent>
      <Flex column>
        <Field name='name'
          component={TextField}
          label='שם' />

        <Field name='link'
          component={TextField}
          label='לינק' />

        <FormControl>
          <InputLabel>כוכבים</InputLabel>
          <Field name='stars'
            component={Select}>
            {[1, 2, 3, 4, 5].map(x => <MenuItem key={`item${x}`} value={x}>
              <StarsRate rate={x} />
            </MenuItem>)}
          </Field>
        </FormControl>
      </Flex>
    </DialogContent>
    <DialogActions>
      <Button color='secondary' onClick={closeDialog}>
        בטל
      </Button>
      <Button
        onClick={handleSubmit}
        variant='contained'
        name='save'
        color='secondary'>
        שמור
      </Button>
    </DialogActions>
  </form>
</Dialog>;
