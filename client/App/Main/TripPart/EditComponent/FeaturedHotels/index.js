import React, { Fragment, Component } from 'react';

import { Flex } from 'reflexbox';

import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import AddIcon from '@material-ui/icons/Add';
import ModeEdit from '@material-ui/icons/ModeEdit';

import DeleteAction from '../../../../common/DeleteAction';

import FeaturedHotel from './FeaturedHotel';
import EditComponent from './EditComponent';

export default class FeaturedHotels extends Component {
  constructor() {
    super();

    this.state = {
      shouldShowNewDialog: false,
      shouldShowUpdateDialog: false
    };

    this.showNewDialog = this.toggleNewDialog.bind(this);
    this.saveNew = this.saveNew.bind(this);
    this.remove = this.remove.bind(this);
    this.update = this.update.bind(this);
  }

  toggleNewDialog(shouldOpen, indicatorName) {
    return () => this.setState(state => ({
      ...state,
      [indicatorName]: shouldOpen
    }));
  }

  saveNew(data) {
    const { input: { onChange, value } } = this.props;

    onChange([...value, data]);

    this.toggleNewDialog(false, 'shouldShowNewDialog')();
  }

  remove(idToRemove) {
    const { input: { onChange, value } } = this.props;
    onChange(value.filter(({ id }) => id !== idToRemove));
  }

  update(data) {
    const { input: { onChange, value } } = this.props;

    const hotelToUpdate = value.find(({ id }) => id === data.id);
    Object.assign(hotelToUpdate, data);
    onChange(value);
    this.toggleNewDialog(false, 'shouldShowUpdateDialog')();
  }

  render() {
    const { input: { value = [] }, label } = this.props;
    const { shouldShowNewDialog, shouldShowUpdateDialog, updateData } = this.state;

    return <Fragment>
      <Typography>{label}</Typography>
      <Flex column>
        <Flex>
          {value
            ? value.map(data =>
              <FeaturedHotel key={data.name}
                name={data.name}
                link={data.link}
                stars={data.stars}>
                <DeleteAction
                  onDelete={() => this.remove(data.id)} name={data.name}
                  type={'מלון'} />
                <IconButton>
                  <ModeEdit onClick={() => {
                    this.setState(state => ({ ...state, updateData: data }));
                    this.toggleNewDialog(true, 'shouldShowUpdateDialog')();
                  }} />
                </IconButton>
              </FeaturedHotel>
            )
            : null}
        </Flex>
        <Flex column justify='space-around' style={{ alignItems: 'center' }}>
          <Tooltip id='add-hotel-icon' title='הוסף מלון'>
            <Button variant='fab' color='secondary'
              mini={true}
              onClick={this.toggleNewDialog(true, 'shouldShowNewDialog')} >
              <AddIcon />
            </Button>
          </Tooltip>
        </Flex>
      </Flex>
      <EditComponent
        close={this.toggleNewDialog(false, 'shouldShowNewDialog')}
        save={this.saveNew}
        isOpen={shouldShowNewDialog} />
      {
        shouldShowUpdateDialog
          ? <EditComponent data={updateData}
            save={this.update}
            close={this.toggleNewDialog(false, 'shouldShowUpdateDialog')}
            isOpen={shouldShowUpdateDialog} />
          : null
      }
    </Fragment >;
  }
}
