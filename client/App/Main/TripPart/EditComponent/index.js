import { connect } from 'react-redux';
import { compose, lifecycle, withStateHandlers } from 'recompose';

import { reduxForm, getFormSyncErrors, getFormMeta } from 'redux-form';

import EditComponent from './EditComponent';

import alerter from '../../../common/alerter';
import redirect from '../../../common/navigation';

import validate from '../../../../../common/validators/tripPart';

export default compose(
  connect(
    state => ({
      countries: state.countries.relevant || [],
      errors: getFormSyncErrors('editTripPartForm')(state),
      meta: getFormMeta('editTripPartForm')(state)
    }),
    (dispath, { action }) => ({
      onSubmit: async function (data) {
        const { error } = await dispath(action(data));
        if (error) {
          alerter({ message: 'save failed' });
        } else {
          alerter({ message: 'save succsed' });
          redirect('/tripPart');
        }
      }
    })
  ),
  reduxForm({
    form: 'editTripPartForm',
    validate,
    initialValues: {
      flight: []
    }
  }),
  lifecycle({
    componentDidMount() {
      const { data, initialize } = this.props;

      if (data) {
        initialize(data);
      }
    }
  }),
  withStateHandlers(
    {
      completed: {},
      activeStep: 0
    },
    {
      handleMoveStep: state => index => ({ ...state, activeStep: index })
    }
  )
)(EditComponent);
