import React, { Fragment } from 'react';

import List from '@material-ui/core/List';

import NewButton from '../../common/new-button';

import ListItem from './ListItem';

export default ({ tripParts = [], onDelete }) => <Fragment>
  <List>
    {tripParts.map(({ ...props }) => <ListItem key={props.id} {...props} onDelete={onDelete} />)}
  </List>
  <NewButton path={'tripPart/new/'} />
</Fragment>;
