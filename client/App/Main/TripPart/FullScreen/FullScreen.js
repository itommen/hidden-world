import React from 'react';

import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';

import { Parser } from 'html-to-react';

import { Flex } from 'reflexbox';

import ImageGallery from 'react-image-gallery';

import FlightIndicator from '../FlightIndicator';

import formatPlace from '../../../common/format-location';

export default ({
  data: { name, start, end, description, days, flight, savedImages = [] } = {},
  loaded
}) => !loaded
  ? <CircularProgress />
  : <Flex column auto>

    <Typography variant='display4' gutterBottom > {name}</Typography >
    <div>
      <Typography>מתחיל ב:</Typography>
      <Typography variant='title' gutterBottom>{formatPlace(start)}</Typography>
    </div>

    <div>
      <Typography>נגמר ב: </Typography>
      <Typography variant='title' gutterBottom>{formatPlace(end)}</Typography>
    </div>
    <div>
      <Typography>משך: </Typography>
      <Typography variant='title' gutterBottom>{days} ימים</Typography>
    </div>
    <FlightIndicator flights={flight} />
    <div>
      <Typography variant='display3'>תיאור היום</Typography>
      {new Parser().parse(description)}
    </div>
    <div>
      <Typography variant='display3'>גלריה</Typography>
      {
        savedImages.length
          ? <ImageGallery
            items={savedImages.map(x => ({
              original: `/uploads/${x}`,
              thumbnail: `/uploads/${x}`
            }))}
            showPlayButton={false}
            showIndex={true} />
          : <Typography>לא נמצאו תמונות מצורפות</Typography>
      }
    </div>
  </Flex >;
