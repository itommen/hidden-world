import React from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import LeftListItemSecondaryAction from '../../common/LeftListItemSecondaryAction';
import ModeEditIcon from '@material-ui/icons/ModeEdit';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

import redirect from '../../common/navigation';

import DeleteAction from '../../common/DeleteAction';

import PrimaryText from './PrimaryText';

export default ({ id, name, days, start, end, flight, onDelete }) => <ListItem button
  divider={true}
  onClick={() => redirect(`tripPart/${id}`)}>
  <ListItemText
    style={{ textAlign: 'right' }}
    primary={<PrimaryText name={name} start={start} end={end} flight={flight} />}
    secondary={`${days} ימים`} />
  <LeftListItemSecondaryAction>
    <Tooltip id='edit-icon' title='ערוך'>
      <IconButton>
        <ModeEditIcon onClick={() => redirect(`tripPart/${id}/edit`)} />
      </IconButton>
    </Tooltip>
    <DeleteAction onDelete={() => onDelete(id)} name={name} type={'חלק טיול'} />
  </LeftListItemSecondaryAction>
</ListItem>;
