import { connect } from 'react-redux';
import { compose } from 'recompose';

import redirect from '../../common/navigation';

import NavigationBar from './NavigationBar';

export default compose(
  connect(
    ({ routing: { locationBeforeTransitions: { pathname } } }) => ({
      pathname
    }),
    () => ({
      handleChange(value) {
        redirect(value);
      }
    })
  )
)(NavigationBar);
