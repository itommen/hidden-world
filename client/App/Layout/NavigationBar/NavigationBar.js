import React from 'react';

import Paper from '@material-ui/core/Paper';

import HomeIcon from '@material-ui/icons/Home';
import PersonIcon from '@material-ui/icons/Person';
import CardTravelIcon from '@material-ui/icons/CardTravel';
import LanguageIcon from '@material-ui/icons/Language';
import HotelIcon from '@material-ui/icons/Hotel';

import BottomNavigation from '@material-ui/core/BottomNavigation';
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction';

const states = [
  {
    label: 'דף הבית',
    url: '',
    icon: <HomeIcon />
  },
  {
    label: 'חלק טיול',
    url: 'tripPart',
    icon: <CardTravelIcon />
  },
  {
    label: 'מלונות',
    url: 'hotels',
    icon: <HotelIcon />
  },
  {
    label: 'ניהול מדינות',
    url: 'manageCountries',
    icon: <LanguageIcon />
  },
  {
    label: 'משתמשים',
    url: 'users',
    icon: <PersonIcon />
  }
];

const selected =
  pathname => states.findIndex(({ url }) => url === (pathname.split('/').filter(p => p)[0] || ''));

export default ({ pathname, handleChange }) => <Paper elevation={20}>
  <BottomNavigation
    value={selected(pathname)}
    showLabels
    onChange={(event, value) => {
      handleChange(`/${states[value].url}`);
    }}>
    {
      states.map(({ label, icon }) => <BottomNavigationAction
        key={label}
        label={label}
        icon={icon} />)
    }
  </BottomNavigation>
</Paper>;
