import React from 'react';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import { Flex } from 'reflexbox';

import Logout from './Logout';

export default ({ isAutorized }) => <AppBar
  position='static'
  title='עולם נסתר'>
  <Toolbar>
    <Flex auto justify='space-between'>
      <img src='/server/assets/logo.png' style={{ width: '17%', height: '9%' }} />
      {isAutorized ? <Logout /> : null}
    </Flex>
  </Toolbar>
</AppBar>;
