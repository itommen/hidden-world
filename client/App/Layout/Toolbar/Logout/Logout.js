import React from 'react';

import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';

export default ({ logout }) => <Tooltip id='logout-icon' title='התנתק'>
  <IconButton aria-label='Logout' onClick={() => logout()} style={{alignSelf:'center'}}>
    <ExitToAppIcon style={{ fontSize: 30, color: 'white' }} />
  </IconButton>
</Tooltip>;
