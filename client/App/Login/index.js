import { connect } from 'react-redux';
import { compose } from 'recompose';

import { reduxForm } from 'redux-form';

import alerter from '../common/alerter';

import validate from '../../../common/validators/login';

import Login from './Login';
import { login as loginAction } from './redux';

export default compose(
  connect(
    () => ({}),
    dispath => ({
      onSubmit: async function (data) {
        const { error } = await dispath(loginAction(data));
        if (!error) {
          return;
        }

        alerter({
          message: error.response.status === 404
            ? 'Login failed. Username or password is inncorect'
            : 'Login failed. Something happened durring login, please try again later'
        });
      }
    })
  ),
  reduxForm({
    form: 'loginForm',
    validate
  })
)(Login);
