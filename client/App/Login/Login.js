import React from 'react';

import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CircularProgress from '@material-ui/core/CircularProgress';

import { Flex } from 'reflexbox';

import { TextField } from 'redux-form-material-ui';

import { Field } from 'redux-form';

export default ({ handleSubmit, submitting }) => <Flex column auto justify='center' align='center'>
  <form onSubmit={handleSubmit}>
    <Card>
      <CardHeader title={'התחבר'} subheader='' />
      <CardContent>
        <Flex column auto>
          <Field name='userName'
            component={TextField}
            label='שם משתמש' />

          <Field name='password'
            component={TextField}
            label='סיסמא' />
        </Flex>
      </CardContent>
      <CardActions>
        <Flex column auto align='center'>
          <Button
            variant='contained'
            type='submit'
            name='login'
            color='secondary'
            disabled={submitting}>
            {
              submitting
                ? <CircularProgress size={20} />
                : 'התחבר'
            }
          </Button>
        </Flex>
      </CardActions>
    </Card>
  </form>
</Flex>;
