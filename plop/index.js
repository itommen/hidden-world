import plur from 'plur';
import uppercamelcase from 'uppercamelcase';
import { camelCase, toUpper } from 'lodash';

export default plop => {
  plop.addHelper('plur', word => plur(word, 2));
  plop.addHelper('plurUpperCase', word => uppercamelcase(plur(word, 2)));
  plop.addHelper('plurCamelCase', word => camelCase(plur(word, 2)));
  plop.addHelper('plurUpperAll', word => toUpper(plur(word, 2)));
  plop.addHelper('upperAll', word => toUpper(word));
  plop.addHelper('mongooseFields', word => word
    .map(({ name }) => `${name}: {
  type: String,
  required: true
}`)
    .join(',\r\n'));
  plop.addHelper('validatorsFields', word => word
    .map(({ name }) => `${name}: [required]`)
    .join(',\r\n'));
  plop.addHelper('fullScreenFieldsParams', word => word.map(({ name }) => name).join(', '));

  plop.setGenerator('api', {
    description: 'Create an api',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'What is the name of your api?',
        validate: ({ length }) => length ? true : 'name is required'
      },
      {
        type: 'editor',
        name: 'fields',
        message: 'What is the fields? sepreated by breakline (String by default)',
        filter: word => [
          ...word.split('\r\n'),
          'name'
        ].map(x => ({
          name: x
        }))
      }
    ],
    actions: () => {
      const actions = [
        // Common
        {
          type: 'add',
          path: 'common/validators/{{ dashCase name}}.js',
          templateFile: 'plop/common/validator.hbs'
        },
        // Server
        {
          type: 'add',
          path: 'server/controllers/{{name}}/index.js',
          templateFile: 'plop/server/index.hbs'
        },
        {
          type: 'add',
          path: 'server/controllers/{{name}}/controller.js',
          templateFile: 'plop/server/controller.hbs'
        },
        {
          type: 'add',
          path: 'server/modals/{{properCase name}}/index.js',
          templateFile: 'plop/server/modal.hbs'
        },
        {
          type: 'modify',
          path: 'server/controllers/index.js',
          pattern: /(\/\/ inject:route-imports)/gi,
          template: '$1\r\nimport {{name}} from \'./{{name}}\';'
        },
        {
          type: 'modify',
          path: 'server/controllers/index.js',
          pattern: /(\/\/ inject:route-usage)/gi,
          template: '$1\r\n  app.use(\'/api/{{plur name}}\', {{name}});'
        },
        {
          type: 'modify',
          path: 'server/modals/index.js',
          pattern: /(\/\/ inject:model-imports)/gi,
          template: '$1\r\nimport \'./{{properCase name}}\';'
        },
        // Client
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/index.js',
          templateFile: 'plop/client/index.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/routes.js',
          templateFile: 'plop/client/routes.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/redux.js',
          templateFile: 'plop/client/redux.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/{{plurUpperCase name}}.js',
          templateFile: 'plop/client/Main.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/EditComponent/index.js',
          templateFile: 'plop/client/EditComponent/index.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/FullScreen/index.js',
          templateFile: 'plop/client/FullScreen/index.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/FullScreen/FullScreen.js',
          templateFile: 'plop/client/FullScreen/FullScreen.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/EditComponent/EditComponent.js',
          templateFile: 'plop/client/EditComponent/EditComponent.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/Insert/index.js',
          templateFile: 'plop/client/Insert/index.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/Edit/index.js',
          templateFile: 'plop/client/Edit/index.hbs'
        },
        {
          type: 'add',
          path: 'client/App/Main//{{plurUpperCase name}}/Edit/Edit.js',
          templateFile: 'plop/client/Edit/Edit.hbs'
        },
        {
          type: 'modify',
          path: 'client/App/Main/index.js',
          pattern: /(\/\/ inject:route-imports)/gi,
          template:
            '$1\r\nimport {{plurUpperCase name}}Routes from \'./{{plurUpperCase name}}/routes\';'
        },
        {
          type: 'modify',
          path: 'client/App/Main/index.js',
          pattern: /(\/\/ inject:route-usage)/gi,
          template: '{ {{plurUpperCase name}}Routes }'
        },
        {
          type: 'modify',
          path: 'client/common/store/reducer.js',
          pattern: /(\/\/ inject:reducers-import)/gi,
          template: `$1\r\nimport {{plurCamelCase name}} 
          from \'../../App/Main/{{plurUpperCase name}}/redux\';`
        },
        {
          type: 'modify',
          path: 'client/common/store/reducer.js',
          pattern: /(\/\/ inject:reducers-usage)/gi,
          template: '{{plurCamelCase name}},'
        }
      ];

      return actions;
    }
  });
};
