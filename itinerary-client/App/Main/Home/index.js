import React from 'react';

import { Flex } from 'reflexbox';

import ImageGallery from 'react-image-gallery';

import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core/styles';

import redirect from '../../../common/navigation';


const styles = {
  cardLayer: {
    display: 'flex',
    position: 'absolute',
    zIndex: 1,
    alignSelf: 'center',
    marginRight: '10%',
    width: '25%',
    height: '60%',
    opacity: 0.8,
    backgroundColor: '#1a1a1ac4',
    color: 'white'
  },
  imageLayer: {
    position: 'absolute',
    zIndex: 2
  },
  typographyColor: {
    color: 'white'
  },
  dividerColor: {
    backgroundColor: 'white'
  },
  buttonColor: {
    backgroundColor: 'orange',
    color: 'white'
  }
}

const images = [
  {
    original: '/itinerary-client/assets/elephant.jpg',
    description: 'פיל בטבע'
  },
  {
    original: '/itinerary-client/assets/jiraffe.jpg',
    description: 'גירפות יפות'
  },
  {
    original: '/itinerary-client/assets/lion.jpg',
    description: 'אריה מפחיד'
  },
  {
    original: '/itinerary-client/assets/panda.jpg',
    description: 'פנדה מתוקה ומקסימה'
  }
];

const Home = ({ classes }) => <Flex>
  <ImageGallery items={images}
    showNav={false}
    showThumbnails={false}
    autoPlay={true}
    showPlayButton={false}
    showFullscreenButton={false}
    slideDuration={2000}
    showBullets={true}
    className={classes.imageLayer} />

  <Card className={classes.cardLayer}>
    <Flex auto column p={2}>
      <CardContent style={{ display: 'flex', flex: '1 1 auto' }}>
        <Flex column auto justify='space-between'>
          <Flex column>
            <Typography variant='display1' className={classes.typographyColor}>שם הטיול</Typography>
            <Typography className={classes.typographyColor}>10 לילות</Typography>

          </Flex>
          <Flex column>
            <Typography className={classes.typographyColor}>
              יש לי מלא תוכן וואי כמה תוכן תוכן תוכן תוכן יש לי מלא תוכן וואי כמה תוכן תוכן תוכן תוכן יש לי מלא תוכן וואי כמה תוכן תוכן תוכן תוכן
            </Typography>
          </Flex>
          <Divider className={classes.dividerColor} />
          <Button
            className={classes.buttonColor}
            onClick={() => redirect('/general')}>
            <Typography className={classes.typographyColor}>היכנס</Typography>
          </Button>
        </Flex>
      </CardContent>
    </Flex>
  </Card>
</Flex>;

export default withStyles(styles)(Home);
