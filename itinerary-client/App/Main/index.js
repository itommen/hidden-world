import React from 'react';
import { Route, IndexRoute } from 'react-router';

import Layout from '../Layout';

import Home from './Home';
import General from './General';

export default <Route component={Layout}>
  <IndexRoute component={Home} />
  <Route path='general' component={General} />
</Route>;
