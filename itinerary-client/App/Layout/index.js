import React, { Fragment } from 'react';
import './layout.less';

export default ({ children }) => <Fragment> {children} </Fragment>;
