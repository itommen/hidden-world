import React from 'react';
import { Route } from 'react-router';

import Layout from './Layout';

import MainRoutes from './Main';

export default <Route path='/'>
  {MainRoutes}
</Route>;
