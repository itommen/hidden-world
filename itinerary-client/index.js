import React from 'react';
import { render } from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';
import { syncHistoryWithStore } from 'react-router-redux';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import { create } from 'jss';
import rtl from 'jss-rtl';
import JssProvider from 'react-jss/lib/JssProvider';
import { createGenerateClassName, jssPreset } from '@material-ui/core/styles';

import { hot } from 'react-hot-loader';

const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

const generateClassName = createGenerateClassName();

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#52c7b8',
      main: '#009688',
      dark: '#00675b',
      contrastText: '#fff'
    },
    secondary: {
      light: '#ffd95b',
      main: '#ffa726',
      dark: '#c77800',
      contrastText: '#000'
    }
  },
  direction: 'rtl',
  typography: {
    fontFamily:
      '-apple-system,system-ui,BlinkMacSystemFont,' +
      '"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    button: {
      fontStyle: 'italic'
    }
  }
});

import { store } from './common/store';
import App from './App';

import '../node_modules/react-image-gallery/styles/css/image-gallery.css';
import 'typeface-roboto';

const history = syncHistoryWithStore(browserHistory, store);

const Root = () => <Provider store={store}>
  <MuiThemeProvider theme={theme}>
    <Router history={history}>
      <JssProvider jss={jss} generateClassName={generateClassName}>
        {App}
      </JssProvider>
    </Router>
  </MuiThemeProvider>
</Provider >;

render(<Root />, document.getElementById('root'));

export default hot(module)(Root);
