import { combineReducers } from 'redux';

import { reducer as formReducer } from 'redux-form';
import { routerReducer as routing } from 'react-router-redux';

// inject:reducers-import

export default combineReducers({
  // inject:reducers-usage
  routing,
  form: formReducer
});
