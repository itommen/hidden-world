import { createStore, applyMiddleware, compose } from 'redux';
import promiseMiddleware, { resolve, reject } from 'redux-simple-promise';
import { multiClientMiddleware } from 'redux-axios-middleware';
import { routerMiddleware } from 'react-router-redux';
import axios from 'axios';

import reducer from './reducer';

const suffixes = {
  successSuffix: resolve(''),
  errorSuffix: reject('')
};

const defaultClient = axios.create({
  baseURL: '/api',
  responseType: 'json'
});

const defaultRequestIntercpetors = [
  ({ getState }, config) => {
    const { auth: { token } } = getState();
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  }
];

const defaultOptions = {
  ...suffixes,
  interceptors: {
    request: defaultRequestIntercpetors
  }
};

const axiosConfig = {
  default: {
    client: defaultClient,
    options: defaultOptions
  }
};

export default (history, initialState = {}) => createStore(
  reducer,
  initialState,
  compose(
    applyMiddleware(
      promiseMiddleware(),
      multiClientMiddleware(axiosConfig),
      routerMiddleware(history)
    )
  ));
