module.exports = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.woff(2)?(\?v=[0-9]+\.[0-9]+\.[0-9]+)?$/,
        use: [{
          loader: 'file-loader',
          options: { limit: 10000 }
        }]
      },
      {
        test: /\.(ttf|eot|svg)(\?v=[0-9]+\.[0-9]+\.[0-9]+)?$/,
        use: ['file-loader']
      },
      {
        test: /\.css?$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.less?$/,
        loader: ['style-loader', 'css-loader', 'less-loader']
      },
      {
        test: /\.html$/,
        loader: ['html-loader']
      }]
  },
  resolve: {
    extensions: ['.js', 'jsx', '.less', '.css', '.html']
  }
};
