const merge = require('webpack-merge');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const common = require('../common');

module.exports = merge(common, {
  entry: [
    './client/index.js'
  ],
  output: {
    path: path.join(__dirname, '..', 'dist', 'client'),
    filename: '[name].[hash].js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './client/index.html'
    })
  ]
});
