const merge = require('webpack-merge');
const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');

const common = require('../common');

module.exports = merge(common, {
  entry: [
    './itinerary-client/index.js'
  ],
  output: {
    path: path.join(__dirname, '..', 'dist', 'itinerary-client'),
    filename: '[name].[hash].js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './itinerary-client/index.html'
    })
  ]
});
